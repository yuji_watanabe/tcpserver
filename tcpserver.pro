#-------------------------------------------------
#
# Project created by QtCreator 2014-04-15T13:44:51
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = tcpserver
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    tcpserver.cpp

HEADERS += \
    tcpserver.h
