﻿#include <QTextStream>
#include <QTcpSocket>
#include <QDebug>
#include "tcpserver.h"

TcpServer::TcpServer(QObject *parent) :
    QObject(parent)
  , m_server(new QTcpServer(this))
{
}

TcpServer::~TcpServer()
{
    delete m_client;
    m_server->close();
}

bool TcpServer::listen()
{
    if (!m_server->listen(QHostAddress::Any, 27220)) {
        return false;
    }
    connect(m_server, &QTcpServer::newConnection, this, [this]{
        m_client = m_server->nextPendingConnection();
        if (m_client) {
            connect(m_client, &QTcpSocket::disconnected, this, []{
                QTextStream(stdout) << "client disconnected." << endl;
            });
            connect(m_client, &QTcpSocket::readyRead, this, [this]{
                QTextStream(stdout) << "request received: " << m_client->readAll() << endl;
                QByteArray hello("hello");
                m_client->write(hello);
            });
            QTextStream(stdout) << m_client->localAddress().toString()
                                   << " is comming." << endl;
        }
    });
    return true;
}
