﻿#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QObject>
#include <QTcpServer>

class TcpServer : public QObject
{
    Q_OBJECT
public:
    explicit TcpServer(QObject *parent = 0);
    ~TcpServer();

    bool listen();

signals:

public slots:

private:
    QTcpServer *m_server;
    QTcpSocket *m_client;

};

#endif // TCPSERVER_H
